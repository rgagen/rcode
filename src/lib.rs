struct BitStream {
    pub bits: Vec<u8>,
}

impl BitStream {
    pub fn len(&self) -> usize {
        self.bits.len()
    }

    pub fn from_hex(hex_string: &str) -> Result<BitStream, &'static str> {
        if !hex_string.chars().all(|x| char::is_ascii_hexdigit(&x)) {
            return Err("Input hex contains non-hex characters");
        }

        if hex_string.len() % 2 != 0 {
            return Err("Input hex length not divisible by 2");
        }

        let hex_string = hex_string.as_bytes();
        let mut bits: Vec<u8> = vec![];

        for i in 0..hex_string.len() {
            if i % 2 == 1 {
                continue;
            };
            let left = BitStream::ascii_hex_to_u8(hex_string[i]) << 4;
            let right = BitStream::ascii_hex_to_u8(hex_string[i + 1]);
            bits.push(left ^ right)
        }

        Ok(BitStream { bits })
    }

    fn ascii_hex_to_u8(hex: u8) -> u8 {
        if hex.is_ascii_digit() {
            return hex - 48;
        } else if hex.is_ascii_uppercase() {
            return hex - 55;
        } else {
            return hex - 87;
        }
    }

    pub fn to_hex(&self) -> String {
        let mut output = String::new();
        for bit in &self.bits {
            let left = (*bit & 0xF0) >> 4;
            let right = *bit & 0x0F;
            output.push(BitStream::u8_to_hex_char(left));
            output.push(BitStream::u8_to_hex_char(right))
        }
        output
    }

    fn u8_to_hex_char(digit: u8) -> char {
        match digit {
            0..=9 => return (digit + 48) as char,
            10..=16 => return (digit + 87) as char,
            _ => panic!("u8 stream contained non-hex value"),
        };
    }

    pub fn from_base64(base64_string: &str) -> Result<BitStream, &'static str> {
        if !base64_string
            .chars()
            .all(|x| char::is_alphanumeric(x) || x == '+' || x == '/' || x == '=')
        {
            return Err("Input string contains non-Base64 characters");
        }

        let base64_string = base64_string.as_bytes();

        let mut bits: Vec<u8> = vec![];

        for i in 0..base64_string.len() {
            if i % 4 != 0 {
                continue;
            }; // skip to next block of 4
            let block = BitStream::base64_block_to_bytes(&base64_string[i..(i + 4)])?;
            for byte in block {
                if let Some(value) = byte {
                    bits.push(value);
                }
            }
        }
        Ok(BitStream { bits })
    }

    fn base64_ascii_to_u8(base: u8) -> u8 {
        if base.is_ascii_digit() {
            return base as u8 + 4;
        } else if base.is_ascii_uppercase() {
            return base as u8 - 65;
        } else if base.is_ascii_lowercase() {
            return base as u8 - 71;
        } else if base == '+' as u8 {
            return 62;
        } else if base == '/' as u8 {
            return 63;
        } else {
            panic!("Base64 Character not valid");
        }
    }

    fn base64_block_to_bytes(block: &[u8]) -> Result<[Option<u8>; 3], &'static str> {
        let mut sextets: [Option<u8>; 4] = [None, None, None, None];
        let mut output: [Option<u8>; 3] = [None, None, None];

        for i in 0..4 {
            sextets[i] = match block.get(i) {
                Some(0x3D) => None,
                None => None,
                Some(value) => Some(*value),
            };
            if sextets[i].is_some() {
                sextets[i] = Some(BitStream::base64_ascii_to_u8(sextets[i].unwrap()));
            };
        }

        if let Some(sextet) = sextets[0] {
            output[0] = Some(sextet << 2);
        } else {
            return Err("First sextet of Base64 block empty");
        }

        if let Some(sextet) = sextets[1] {
            output[0] = Some(output[0].unwrap() ^ (sextet >> 4));
            output[1] = Some(sextet << 4);
        } else {
            return Err("Second sextet of Base64 block empty");
        }

        if let Some(sextet) = sextets[2] {
            output[1] = Some(output[1].unwrap() ^ (sextet >> 2));
            output[2] = Some(sextet << 6);
        } else {
            output[1] = None;
            return Ok(output);
        }

        if let Some(sextet) = sextets[3] {
            output[2] = Some(output[2].unwrap() ^ sextet);
        } else {
            output[2] = None;
        }

        Ok(output)
    }

    pub fn to_base64(&self) -> String {
        let mut output = String::new();

        for i in 0..self.bits.len() {
            if i % 3 != 0 {
                continue;
            };

            let mut block: [Option<u8>; 4] = [None; 4];

            if let Some(bit) = self.bits.get(i) {
                block[0] = Some((bit & 0xFC) >> 2);
                block[1] = Some((bit & 0x03) << 4);
            } else {
                break;
            }

            if let Some(bit) = self.bits.get(i + 1) {
                block[1] = Some(block[1].unwrap() ^ (bit >> 4));
                block[2] = Some((bit & 0x0F) << 2);
            }

            if let Some(bit) = self.bits.get(i + 2) {
                block[2] = Some(block[2].unwrap() ^ (bit >> 6));
                block[3] = Some(bit & 0x3F);
            }

            for i in 0..4 {
                if let Some(sextet) = block[i] {
                    output.push(BitStream::base64_index_to_ascii(sextet));
                } else {
                    output.push('=');
                }
            }
        }
        output
    }

    fn base64_index_to_ascii(index: u8) -> char {
        match index {
            0..=25 => (index + 65) as char,
            26..=51 => (index + 71) as char,
            52..=61 => (index - 4) as char,
            62 => '+',
            63 => '/',
            _ => panic!("Invalid base64 index"),
        }
    }

    fn xor(&self, target: &BitStream) -> Result<BitStream, &'static str> {
        let mut xored_bits: Vec<u8> = vec![];
        if self.len() != target.len() {
            return Err("Bitstream lengths must be equal for XOR");
        };
        for i in 0..self.len() {
            xored_bits.push(self.bits[i] ^ target.bits[i]);
        }
        Ok(BitStream{ bits: xored_bits })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn hex_in_and_out() {
        let hex_string = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d";
        let bits = BitStream::from_hex(hex_string).expect("BitStream creation failed!");
        assert_eq!(
            hex_string.len() / 2,
            bits.len(),
            "Stored bits should be half the length of the input hex"
        );
        assert_eq!(
            hex_string,
            bits.to_hex(),
            "Input and output hexes did not match"
        );
    }

    #[test]
    fn base64_in_and_out() {
        let base64_string = "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t";
        let bits = BitStream::from_base64(base64_string).expect("BitStream creation failed!");
        assert_eq!(base64_string.len() * 6, bits.len() * 8);
        assert_eq!(base64_string, bits.to_base64());
    }

    #[test]
    fn challenge1() {
        let hex_string = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d";
        let base64_string = "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t";
        let bits = BitStream::from_hex(hex_string).expect("BitStream creation failed!");
        assert_eq!(base64_string, bits.to_base64());
    }

    #[test]
    fn challenge2() {
        let hex_string1 = BitStream::from_hex("1c0111001f010100061a024b53535009181c")
            .expect("Bitstream creation failed");
        let hex_string2 = BitStream::from_hex("686974207468652062756c6c277320657965")
            .expect("Bitstream creation failed");
        let output = hex_string1.xor(&hex_string2)
            .expect("XOR failed");
        let expected_result = "746865206b696420646f6e277420706c6179";
        assert_eq!(output.to_hex(), expected_result);
    }
}
